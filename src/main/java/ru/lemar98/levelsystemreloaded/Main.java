package ru.lemar98.levelsystemreloaded;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.commands.LevelCommand;
import ru.lemar98.levelsystemreloaded.listeners.Join;
import ru.lemar98.levelsystemreloaded.listeners.Quit;
import ru.lemar98.levelsystemreloaded.system.LevelManager;
import ru.lemar98.levelsystemreloaded.util.ChatUtils;

import java.util.UUID;

public class Main extends JavaPlugin {

    private static ChatUtils chatUtils;
    private static Main instance;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        chatUtils = new ChatUtils(this);
        instance = this;

        Bukkit.getPluginManager().registerEvents(new Join(this), this);
        Bukkit.getPluginManager().registerEvents(new Quit(this), this);

        getCommand("level").setExecutor(new LevelCommand(this));

        /**
         * Запускаем шедулер с уровнями
         */
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Scheduler(this),20L, 20L*60L*getConfig().getInt("Settings.updateTime"));
    }

    @Override
    public void onDisable() {
        /**
         * Сохраняем все данные при выключении
         */
        for(UUID uuid : LevelManager.levelMap.keySet()) {
            LevelManager levelManager = new LevelManager(uuid, this);
            levelManager.removePlayer();
        }
    }

    public static ChatUtils getChatUtils() {
        return chatUtils;
    }

    public static Main getInstance() {
        return instance;
    }
}
