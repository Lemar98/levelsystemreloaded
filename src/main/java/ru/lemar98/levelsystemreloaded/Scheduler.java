package ru.lemar98.levelsystemreloaded;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.system.LevelManager;

import java.util.UUID;

public class Scheduler implements Runnable {

    private JavaPlugin plugin;

    public Scheduler(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for(UUID uuid : LevelManager.levelMap.keySet()) {
            LevelManager levelManager = new LevelManager(uuid, plugin);
            Player player = Bukkit.getPlayer(uuid);
            /**
             * Удваиваем время игроку, если у него перм
             * Сорян за небольшой костыль, лень делать отдельный метод ¯\_(ツ)_/¯
             */
            if(player.hasPermission("level.multiplyTime")) {
                levelManager.incrementCollectTime();
            }

            levelManager.incrementCollectTime();
            levelManager.incrementAllCollectTime();
            if(levelManager.getCollectTime() >= levelManager.getRequireMinutesForLvlup()) {
                levelManager.LevelUp();
                levelManager.incrementNextLevel();
                levelManager.multiplyRequireTime();
                levelManager.resetCollectTime();
                /**
                 * Если сообщение разрешено в конфиге, выводим
                 */
                if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("lvlup"))) {
                    player.sendMessage(Main.getChatUtils().getMessage("lvlup").replaceAll("; true", ""));
                }
                if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("lvlUpTitle"))) {
                    player.sendTitle(Main.getChatUtils().getMessage("lvlUpTitle")
                            .split(":")[0].replace("%lvl%", String.valueOf(levelManager.getPlayerLevel()))
                            .replaceAll("; true", "")
                            ,Main.getChatUtils().getMessage("lvlUpTitle")
                            .split(":")[1].replace("%lvl%", String.valueOf(levelManager.getPlayerLevel()))
                            .replaceAll("; true", ""), 20, 40, 20);
                }
            }
        }
    }
}
