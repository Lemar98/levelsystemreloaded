package ru.lemar98.levelsystemreloaded.api;

import ru.lemar98.levelsystemreloaded.Main;
import ru.lemar98.levelsystemreloaded.system.LevelManager;

import java.util.UUID;

public class Api {

    private UUID uuid;

    public Api(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Returns player level
     *
     * Вернет лвл игрока
     * @return int - лвл игрока
     */
    public int getLevel() {
        return new LevelManager(uuid, Main.getInstance()).getPlayerLevel();
    }

    /**
     * Returns the next player level
     *
     * Вернет следующий уровень для игрока
     * @return int - следующий лвл для игрока
     */
    public int getNextLvl() {
        return new LevelManager(uuid, Main.getInstance()).getNextLevelForPlayer();
    }

    /**
     * Returns completed minutes for the next level
     *
     * Вернет отыгранные минуты на данном этапе игрока
     * @return int - отыгранные минуты на данный момент
     */
    public int getMinutesForLvl() {
        return new LevelManager(uuid, Main.getInstance()).getCollectTime();
    }

    /**
     * Returns the required minutes to level up
     *
     * Вернет требуемое кол-во минут для лвл-апа
     * @return int - требуемые минут для лвл-апа
     */
    public int getRequireMinutesForLvlup() {
        return new LevelManager(uuid, Main.getInstance()).getRequireMinutesForLvlup();
    }

    /**
     * Returns the total time played by the player since the installation of the plugin
     *
     * Вернет общие отыгранные игроком минуты
     * @return int - сколько отыграл игрок в общем
     */
    public int getTotalPlayCount() {
        return new LevelManager(uuid, Main.getInstance()).getAllCollectTime();
    }
}
