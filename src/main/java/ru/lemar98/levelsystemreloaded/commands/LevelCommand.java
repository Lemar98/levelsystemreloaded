package ru.lemar98.levelsystemreloaded.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.Main;
import ru.lemar98.levelsystemreloaded.system.LevelManager;

public class LevelCommand implements CommandExecutor {

    private JavaPlugin plugin;

    public LevelCommand(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                LevelManager levelManager = new LevelManager(player.getUniqueId(), plugin);
                for(String text : plugin.getConfig().getStringList("LevelInfoFormat")) {
                    player.sendMessage(Main.getChatUtils().setColor(text
                            .replace("%player%", player.getName()))
                            .replace("%lvl%", String.valueOf(levelManager.getPlayerLevel()))
                            .replace("%nextLvl%", String.valueOf(levelManager.getNextLevelForPlayer()))
                            .replace("%require%", String.valueOf(levelManager.getRequireMinutesForLvlup()))
                            .replace("%collectTime%", String.valueOf(levelManager.getCollectTime()))
                            .replace("%totalTime%", String.valueOf(levelManager.getAllCollectTime())));
                }
                return true;

            } else if(args.length == 2 && args[0].equals("reset")) {
                if(!player.hasPermission("level.admin")) {
                    if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("noPerm"))) {
                        player.sendMessage(Main.getChatUtils().getMessage("noPerm").replaceAll("; true", ""));
                    }
                    return true;
                }
                if(Bukkit.getPlayerExact(args[1]) == null) {
                    if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("nonPlayer"))) {
                        player.sendMessage(Main.getChatUtils().getMessage("nonPlayer").replaceAll("; true", ""));
                    }
                    return true;
                }
                Player target = Bukkit.getPlayerExact(args[1]);
                LevelManager levelManager = new LevelManager(target.getUniqueId(), plugin);
                levelManager.resetAll();
                if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("yourProgressHasReset"))) {
                    target.sendMessage(Main.getChatUtils().getMessage("yourProgressHasReset").replaceAll("; true", ""));
                }
                if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("youCrazy"))) {
                    player.sendMessage(Main.getChatUtils().getMessage("youCrazy").replaceAll("; true", ""));
                }
                return true;
            } else if(args.length == 1) {
                if(!player.hasPermission("level.checkOther")) {
                    if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("noPerm"))) {
                        player.sendMessage(Main.getChatUtils().getMessage("noPerm").replaceAll("; true", ""));
                    }
                    return true;
                }
                if(Bukkit.getPlayerExact(args[0]) == null) {
                    if(Main.getChatUtils().allowMessage(Main.getChatUtils().getMessage("nonPlayer"))) {
                        player.sendMessage(Main.getChatUtils().getMessage("nonPlayer").replaceAll("; true", ""));
                    }
                    return true;
                }
                Player target = Bukkit.getPlayerExact(args[0]);
                LevelManager levelManager = new LevelManager(target.getUniqueId(), plugin);
                for(String text : plugin.getConfig().getStringList("LevelInfoFormat")) {
                    player.sendMessage(Main.getChatUtils().setColor(text
                            .replace("%player%", target.getName()))
                            .replace("%lvl%", String.valueOf(levelManager.getPlayerLevel()))
                            .replace("%nextLvl%", String.valueOf(levelManager.getNextLevelForPlayer()))
                            .replace("%require%", String.valueOf(levelManager.getRequireMinutesForLvlup()))
                            .replace("%collectTime%", String.valueOf(levelManager.getCollectTime()))
                            .replace("%totalTime%", String.valueOf(levelManager.getAllCollectTime())));
                }
                return true;
            }
        } else {
            sender.sendMessage(Main.getChatUtils().setColor("&cИз консоли низя ^_^"));
            return true;
        }
        return true;
    }
}
