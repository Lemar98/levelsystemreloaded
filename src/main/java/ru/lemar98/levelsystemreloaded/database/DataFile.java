package ru.lemar98.levelsystemreloaded.database;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class DataFile {

    private YamlConfiguration yml;
    private File file;

    public DataFile(String name, JavaPlugin plugin) {
        this.file = new File(plugin.getDataFolder()+"/Database/", name + ".yml");
        this.yml = YamlConfiguration.loadConfiguration(this.file);
        this.file.getParentFile().mkdirs();
    }

    public FileConfiguration getFile() {
        return this.yml;
    }

    public void save() {
        try {
            this.yml.save(this.file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
