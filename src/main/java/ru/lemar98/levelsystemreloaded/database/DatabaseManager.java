package ru.lemar98.levelsystemreloaded.database;

import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.system.Level;

import java.io.File;
import java.util.UUID;

public class DatabaseManager {

    private JavaPlugin plugin;

    public DatabaseManager(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public Level getPlayerInfo(UUID uuid) {
        DataFile dataFile = new DataFile(uuid.toString(), plugin);
        return new Level(
                uuid, dataFile.getFile().getInt("level"),
                dataFile.getFile().getInt("nextLevel"), dataFile.getFile().getInt("requireTime"),
                dataFile.getFile().getInt("collectTime"), dataFile.getFile().getInt("allCollectTime")
        );
    }

    public void addNewPlayerDatFile(Level level) {
        DataFile dataFile = new DataFile(level.getPlayerUuid().toString(), plugin);
        dataFile.getFile().set("level", level.getLevel());
        dataFile.getFile().set("nextLevel", level.getNextLevel());
        dataFile.getFile().set("requireTime", level.getRequireTime());
        dataFile.getFile().set("collectTime", level.getCollectTime());
        dataFile.getFile().set("allCollectTime", level.getAllCollectTime());
        dataFile.save();
    }

    public static boolean exist(String name, JavaPlugin plugin) {
        File file = new File(plugin.getDataFolder()+"/Database/", name + ".yml");
        return file.exists();
    }
}

