package ru.lemar98.levelsystemreloaded.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.database.DatabaseManager;
import ru.lemar98.levelsystemreloaded.system.Level;
import ru.lemar98.levelsystemreloaded.system.LevelManager;

import java.util.UUID;

public class Join implements Listener {

    private JavaPlugin plugin;

    public Join(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        UUID playerUuid = event.getPlayer().getUniqueId();
        if(!DatabaseManager.exist(playerUuid.toString(), plugin)) {
            Level level = new Level(playerUuid, 1, 2, 60, 0, 0);
            LevelManager.levelMap.put(playerUuid, level);
            DatabaseManager databaseManager = new DatabaseManager(plugin);
            databaseManager.addNewPlayerDatFile(level);
        } else {
            DatabaseManager databaseManager = new DatabaseManager(plugin);
            Level level = databaseManager.getPlayerInfo(playerUuid);
            LevelManager.levelMap.put(playerUuid, level);
        }
    }
}
