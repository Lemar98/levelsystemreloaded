package ru.lemar98.levelsystemreloaded.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.system.LevelManager;

import java.util.UUID;

public class Quit implements Listener {

    private JavaPlugin plugin;

    public Quit(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        if(LevelManager.levelMap.containsKey(uuid)) {
            LevelManager levelManager = new LevelManager(uuid, plugin);
            levelManager.removePlayer();
        }
    }
}
