package ru.lemar98.levelsystemreloaded.system;

import java.util.UUID;

public class Level {

    private UUID playerUuid;
    private int level;
    private int nextLevel;
    private int requireTime;
    private int CollectTime;
    private int allCollectTime;

    public Level(UUID playerUuid, int level, int nextLevel, int requireTime, int collectTime, int allCollectTime) {
        this.playerUuid = playerUuid;
        this.level = level;
        this.nextLevel = nextLevel;
        this.requireTime = requireTime;
        CollectTime = collectTime;
        this.allCollectTime = allCollectTime;
    }

    public UUID getPlayerUuid() {
        return playerUuid;
    }

    public int getLevel() {
        return level;
    }

    public int getNextLevel() {
        return nextLevel;
    }

    public int getRequireTime() {
        return requireTime;
    }

    public int getCollectTime() {
        return CollectTime;
    }

    public int getAllCollectTime() {
        return allCollectTime;
    }

    public void LevelUp() {
        level++;
    }

    public void incrementNextLevel() {
        nextLevel++;
    }

    public void incrementRequireTime() {
        requireTime = requireTime * 2;
    }

    public void incrementCollectTime() {
        CollectTime++;
    }

    public void incrementAllCollectTime() {
        allCollectTime++;
    }

    public void resetCollectTime() {
        CollectTime = 0;
    }

    public void resetAll() {
        level = 1;
        nextLevel = 2;
        requireTime = 60;
        CollectTime = 0;
        allCollectTime = 0;
    }
}
