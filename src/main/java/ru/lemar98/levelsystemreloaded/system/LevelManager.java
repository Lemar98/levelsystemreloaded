package ru.lemar98.levelsystemreloaded.system;

import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.levelsystemreloaded.database.DataFile;
import ru.lemar98.levelsystemreloaded.database.DatabaseManager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LevelManager {

    public static Map<UUID, Level> levelMap;
    private JavaPlugin plugin;
    private UUID playerUuid;
    private DataFile playerDataFile;

    public LevelManager(UUID playerUuid, JavaPlugin plugin) {
        this.playerUuid = playerUuid;
        this.plugin = plugin;
        if(DatabaseManager.exist(playerUuid.toString(), plugin)) {
            this.playerDataFile = new DataFile(playerUuid.toString(), plugin);
        }
    }

    /**
     * Удаляет игрока из памяти и сохраняет несохраненную информацию
     */
    public void removePlayer() {
        if(levelMap.containsKey(playerUuid)) {
            playerDataFile.getFile().set("level", levelMap.get(playerUuid).getLevel());
            playerDataFile.getFile().set("nextLevel", levelMap.get(playerUuid).getNextLevel());
            playerDataFile.getFile().set("requireTime", levelMap.get(playerUuid).getRequireTime());
            playerDataFile.getFile().set("collectTime", levelMap.get(playerUuid).getCollectTime());
            playerDataFile.getFile().set("allCollectTime", levelMap.get(playerUuid).getAllCollectTime());
            playerDataFile.save();
            levelMap.remove(playerUuid);
        }
    }

    /**
     * Возвращает уровень
     * @return уровень игрока
     */
    public int getPlayerLevel() {
        return levelMap.get(playerUuid).getLevel();
    }

    /**
     * Возвращает следующий уровень
     * @return следующий уровень для игрока
     */
    public int getNextLevelForPlayer() {
        return levelMap.get(playerUuid).getNextLevel();
    }

    /**
     * Возвращает требуемые минуты для лвл-апа
     * @return количество часов для повышения уровня
     */
    public int getRequireMinutesForLvlup() {
        return levelMap.get(playerUuid).getRequireTime();
    }

    /**
     * Возвращает "отсиженное" игроком время для следуюущего уровня
     * @return кол-во минут, "отсиженных" игроком на сервере для селдующего лвл
     */
    public int getCollectTime() {
        return levelMap.get(playerUuid).getCollectTime();
    }

    /**
     * Возвращает общее количество времени, "отсиженное" игроком
     * @return кол-во минут, "отсиженных" игроком на сверере в общем (с момента работы плагина)
     */
    public int getAllCollectTime() {
        return levelMap.get(playerUuid).getAllCollectTime();
    }

    /**
     * Увеличивает уровень игрока на 1
     */
    public void LevelUp() {
        levelMap.get(playerUuid).LevelUp();
    }

    /**
     * Увеличивает следующий уровень игрока на 1
     */
    public void incrementNextLevel() {
        levelMap.get(playerUuid).incrementNextLevel();
    }

    /**
     * Увеличивает время игрока на 1 минуту
     */
    public void incrementCollectTime() {
        levelMap.get(playerUuid).incrementCollectTime();
    }

    /**
     * Увеличивает общее время игрока на 1 минуту
     */
    public void incrementAllCollectTime() {
        levelMap.get(playerUuid).incrementAllCollectTime();
    }

    /**
     * Умножает требуемые минуты для повышения уровня на 2
     */
    public void multiplyRequireTime() {
        levelMap.get(playerUuid).incrementRequireTime();
    }

    /**
     * Сбросить наигранное время для уровня
     */
    public void resetCollectTime() {
        levelMap.get(playerUuid).resetCollectTime();
    }

    /**
     * Сбрасывает все данные игрока (уровень, время, etc.)
     */
    public void resetAll() {
        levelMap.get(playerUuid).resetAll();
    }

    static {
        levelMap = new HashMap<>();
    }
}
