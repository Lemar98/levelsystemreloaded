package ru.lemar98.levelsystemreloaded.util;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatUtils {

    private JavaPlugin plugin;

    public ChatUtils(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public String setColor(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    public String getMessage(String message) {
        return setColor(plugin.getConfig().getString("Messages."+message));
    }

    public boolean allowMessage(String msg) {
        return msg.contains("; true");
    }
}
